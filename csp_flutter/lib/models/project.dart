class Project {
    late final int? id;
    late final String? name;
    late final String? description;
    late final int? createdBy;
    late final int? assignedTo;

    Project({ 
        this.id, 
        this.name,
        this.description,
        this.createdBy,
        this.assignedTo
    });

    factory Project.fromJson(Map<String, dynamic> json) {
        return Project(
            id: json['id'],
            name: json['name'],
            description: json['description'],
            createdBy: json['createdBy'],
            assignedTo: json['assignedTo']
        );
    }
}