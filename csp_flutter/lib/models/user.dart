class User {
    late final int? id;
    late final String? email;
    late final String? accessToken;
    late final String? designation;

    User({ 
        this.id, 
        this.email,
        this.accessToken,
        this.designation
    });

    factory User.fromJson(Map<String, dynamic> json) {
        return User(
            id: json['id'],
            email: json['email'],
            accessToken: json['accessToken'],
            designation: json['designation']
        );
    }
}