class Task {
    late final int id;
    late final String title;
    late final String description;
    late final String? imageLocation;
    late final String? status;
    late final int projectId;
    late final int? assignedTo;
    late final int? reviewedBy;

    Task({ 
        required this.id, 
        required this.title,
        required this.description,
        required this.imageLocation,
        required this.status,
        required this.projectId,
        this.assignedTo,
        this.reviewedBy
    });

    factory Task.fromJson(Map<String, dynamic> json) {
        return Task(
            id: json['id'],
            title: json['title'],
            description: json['description'],
            imageLocation: json['imageLocation'],
            status: json['status'],
            projectId: json['projectId'],
            assignedTo: json['assignedTo'],
            reviewedBy: json['reviewedBy']
        );
    }
}