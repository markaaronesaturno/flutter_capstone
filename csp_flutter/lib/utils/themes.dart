import 'package:flutter/material.dart';

var btnDefaultTheme = ElevatedButton.styleFrom(
    primary: Colors.white, // background color
    side: BorderSide(width: 1.0, color: Colors.black),
    onPrimary: Colors.black // text color
);

var btnRed = ElevatedButton.styleFrom(
    primary: Colors.red[900],
    );

var btnWhite = ElevatedButton.styleFrom(
    primary: Colors.white,
    side: BorderSide(width: 1.0, color: Colors.black,)  
    );