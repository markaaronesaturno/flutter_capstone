import 'package:csp_flutter/utils/api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:csp_flutter/utils/functions.dart';

import '/widgets/app_drawer.dart';
import '/widgets/add_project_dialog.dart';
import '/models/project.dart';
import '/widgets/project_card.dart';
import '/providers/user_provider.dart';

class ProjectListScreen extends StatefulWidget {
    @override
    _ProjectListScreen createState() => _ProjectListScreen();
}

class _ProjectListScreen extends State<ProjectListScreen> {
    Future<List<Project>>? _futureProjects;

    final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

    void _reloadProjects() {
        final String? accessToken = context.read<UserProvider>().accessToken;
    
        setState((){
        _futureProjects = API(accessToken).getProjects().catchError((error){
                showSnackBar(context, error.message);    
        });
    }); 

    }

    Widget _showProjects(List? projects) {
        var cardProjects = projects!.map((project) => ProjectCard(project, _reloadProjects)).toList();

        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
                _reloadProjects();
            },
            child: ListView(
                children: cardProjects
            ), 
        );
    }

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;

            setState((){
                _futureProjects = API(accessToken).getProjects().catchError((error){
                     showSnackBar(context, error.message);    
                });
            });         
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget fabAddProject = FloatingActionButton(
            child: Icon(Icons.add),
                backgroundColor:Colors.red[700],
            // Update FAB theme according to shown example.
            onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) => AddProjectDialog()
                ).then((value) {
                    _reloadProjects();
                });
            },
        );

        Widget projectListView = FutureBuilder(
            future: _futureProjects,
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                    return _showProjects(snapshot.data as List); // Show the projects here.
                } else {
                    return Center(
                        child: CircularProgressIndicator()
                    );
                }
            }
        );

        return Scaffold(
            appBar: AppBar(
                title: Text('Project List'),
                backgroundColor: Color.fromRGBO(20, 45, 68, 1)
                ),
            endDrawer: AppDrawer(),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: projectListView
            ),
            floatingActionButton: (designation == 'contractor') ? fabAddProject : null,
        );
    }
}