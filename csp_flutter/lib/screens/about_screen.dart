import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Text("Developer Profile"),
            ),
            body: Center(
                child: 
                ListView(
                    children: [
                        Stack(
                            alignment: Alignment.center,
                            children: [
                                Padding(
                                    padding: EdgeInsets.only(top: 30),
                                    child: CircleAvatar(
                                        radius: 80, 
                                        backgroundColor: Colors.white, 
                                        backgroundImage:  AssetImage('assets/2x2.png')   
                                    )
                                ),
                            ],
                        ),
                        SizedBox(height: 20,),
                        ListTile(
                            title: Text(
                                'Mark Saturno',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 24
                                ),
                                textAlign: TextAlign.center,
                            ),
                            subtitle: Text(
                                'Junior Flutter Developer',
                                textAlign: TextAlign.center,
                            ),
                        ),

                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                                Padding(
                                    padding: EdgeInsets.only(right:8),
                                    child: Material(
                                        color: Colors.white,
                                        child: Center(
                                            child: Ink(
                                                decoration: const ShapeDecoration(
                                                color: Colors.lightBlue,
                                                shape: CircleBorder(),
                                                ),
                                                child: IconButton(
                                                    icon: const Icon(Icons.mail),
                                                    color: Colors.white,
                                                    onPressed: () {
                                                        showDialog<String>(
                                                            context: context,
                                                            builder: (BuildContext context) => AlertDialog(
                                                            title: const Text('You can reach out to me:'),
                                                            content: const Text('markaaronesaturno@gmail.com'),
                                                            actions: <Widget>[
                                                                TextButton(
                                                                onPressed: () => Navigator.pop(context, 'OK'),
                                                                child: const Text('OK'),
                                                                ),
                                                            ],
                                                            ),
                                                        );
                                                    },
                                                ),
                                            ),
                                        ),
                                    ),
                                ),


                                Material(
                                    color: Colors.white,
                                    child: Center(
                                        child: Ink(
                                            decoration: const ShapeDecoration(
                                            color: Colors.lightBlue,
                                            shape: CircleBorder(),
                                            ),
                                            child: IconButton(
                                            icon: const Icon(Icons.phone),
                                            color: Colors.white,
                                            onPressed: () {
                                                    showDialog<String>(
                                                        context: context,
                                                        builder: (BuildContext context) => AlertDialog(
                                                        title: const Text('Want to Talk?'),
                                                        content: const Text('0995-012-8343'),
                                                        actions: <Widget>[
                                                            TextButton(
                                                            onPressed: () => Navigator.pop(context, 'OK'),
                                                            child: const Text('OK'),
                                                            ),
                                                        ],
                                                        ),
                                                    );

                                                },
                                            ),
                                        ),
                                    ),
                                ),
                            ],
                        ),
                        
                        Container(
                            padding: EdgeInsets.only(left:18),
                            child: Text(
                                'About Me',
                                style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold
                                ),
                                textAlign: TextAlign.left,
                            ),
                        ),

                        Container(
                            margin: EdgeInsets.all(3),
                            padding: EdgeInsets.all(8),
                            child: 
                                Column(
                                    children: [
                                        Padding(
                                            padding: EdgeInsets.all(8),
                                            child: Text(
                                                'I am a fresh graduate in the field of Mechanical Engineer at Don Bosco Technical College, Batch 2020. My hobbies include designing different things with the use of design tool like SolidWorks and Autocad. I also like watching netflix and to learn different things everyday.',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                ),
                                                textAlign: TextAlign.left,
                                                
                                            ),   
                                        )      
                                    ],
                                ),
                        ),

                        Container(
                            child: Text(
                                'Skills & Technologies',
                                style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold
                                ),
                                textAlign: TextAlign.center,
                            ),
                        ),

                        Container(
                            child: Center(
                                child:Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                        Image.asset('assets/html.jpg',width: 100),
                                        Image.asset('assets/css1.png',width: 80),
                                        Image.asset('assets/js.png',width: 80),
                                        Image.asset('assets/dart.png',width: 85),
                                    ],
                                )
                            )
                        ),

                        Container(
                            child: Center(
                                child:Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                        Image.asset('assets/react.png',width: 80),
                                        Image.asset('assets/flutter.png',width: 80),
                                        Image.asset('assets/node.png',width: 80),
                                    ],
                                )
                            )
                        ),

            

                    ],
                )
            )    
        );
        
    }
}


// Short Intro
// Skills
// Interests
// Contact Info (LinkedIn, Portfolio Website)



