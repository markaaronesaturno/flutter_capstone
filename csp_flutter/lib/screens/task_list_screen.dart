import 'package:csp_flutter/utils/api.dart';
import 'package:csp_flutter/widgets/add_task_dialog.dart';
import 'package:csp_flutter/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/models/task.dart';
import '/widgets/task_card.dart';
import '/providers/user_provider.dart';


class TaskListScreen extends StatefulWidget {
    final int? _projectId;

    TaskListScreen(this._projectId);

    @override
    _TaskListScreen createState() => _TaskListScreen();
}

class _TaskListScreen extends State<TaskListScreen> {
    Future<List<Task>>? _futureTasks;

    final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

    void _reloadTasks() {

        final String? accessToken = context.read<UserProvider>().accessToken;

        setState((){
            _futureTasks = API(accessToken).getTasks(widget._projectId).catchError((error){
            showSnackBar(context, error.message);    
            });
        });

    }

    Widget _showTasks(List? tasks) {
        var cardTasks = tasks!.map((task) => TaskCard(task, _reloadTasks)).toList();

        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
                _reloadTasks();
            },
            child: ListView(
                children: cardTasks
            ), 
        );
    }

    @override
    void initState() {
        super.initState();
        
        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;
            
            setState(() {
              _futureTasks = API(accessToken).getTasks(widget._projectId).catchError((error){
                showSnackBar(context, error.message);
                });
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;
        final String? accessToken = context.read<UserProvider>().accessToken;

        Widget fabAddTask = FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Color.fromRGBO(205, 23, 25, 1),
            foregroundColor: Colors.white,
            onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) => AddTaskDialog(widget._projectId)
                ).then((value) {
                    setState((){
                        _futureTasks = API(accessToken).getTasks(widget._projectId).catchError((error){
                        showSnackBar(context, error.message);    
                        });
                    });
                });
            },
        );

        Widget taskListView = FutureBuilder(
            future: _futureTasks,
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                    return _showTasks(snapshot.data as List);
                } else {
                    return Center (
                        child: CircularProgressIndicator()
                    );
                }
            }
        );

        return Scaffold(
            appBar: AppBar(
                title: Text('Project Task List'),
                backgroundColor: Color.fromRGBO(20, 45, 68, 1)
            ),
            body: Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: taskListView
            ),
            floatingActionButton: (designation == 'subcontractor') ? fabAddTask : null,
        );
    }
}