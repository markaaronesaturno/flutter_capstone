import 'package:csp_flutter/screens/about_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/providers/user_provider.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setDesignation = Provider.of<UserProvider>(context, listen: false).setDesignation;

        return Drawer(
            child: Container(
                width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    SizedBox(height: 50.0),
                                    Image.asset(
                                        'assets/ffuf-logo.png',
                                        height: 30,
                                        width: 130,
                                        color: Colors.white
                                    
                                    
                                    ), // Modify width and color of Image widget to match given sample.
                                    Container(
                                        margin: EdgeInsets.only(top: 16.0),
                                        child: Column(
                                            children: [
                                                Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: Text('FFUF Project Management', style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                                    
                                                ),
                                                Align(
                                                    alignment: Alignment.centerLeft,
                                                    child: Text('Made by FFUF Internal Dev Team', style: TextStyle(color: Colors.white))
                                                )
                                            ]
                                        )
                                    )
                                ]
                            )
                        ),
                        Spacer(),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                ListTile(
                                    title: Text('About me', style: TextStyle(color: Colors.white)),
                                    onTap: () async {

                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => AboutScreen()),
                                        );
                                           
                                    }
                                ),


                                ListTile(
                                    title: Text('Logout', style: TextStyle(color: Colors.white)),
                                    onTap: () async {
                                        
                                        SharedPreferences prefs = await SharedPreferences.getInstance();

                                        prefs.remove('accessToken');
                                        prefs.remove('userId');

                                        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                        Provider.of<UserProvider>(context, listen: false).setDesignation(null);

                                        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);

                                    }
                                ),
                            ]
                        )
                    ]
                )
            )
        );
    }
}