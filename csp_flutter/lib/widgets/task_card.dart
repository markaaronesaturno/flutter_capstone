import 'package:csp_flutter/utils/api.dart';
import 'package:csp_flutter/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/themes.dart';
import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';

class TaskCard extends StatefulWidget {
    final Task _task;
    final Function _reloadTasks;

    TaskCard(this._task, this._reloadTasks);

    @override
    _TaskCard createState() => _TaskCard();
}



class _TaskCard extends State<TaskCard> {
    String? taskStatus;
    
    updateTask(BuildContext context, String status){
        final String? accessToken = context.read<UserProvider>().accessToken;
        API(accessToken).updateTaskStatus(taskId: widget._task.id, status: status)
        .then((value){
            if(value == true){
                setState(() {
                  taskStatus = status;  
                });
            } 
        })
        .catchError((error){
        showSnackBar(context, error.message);       
        });
    }

    void initState() {
        taskStatus = widget._task.status!;
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget rowTaskInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // Modify both the main and cross axis alignment.
                        children: [
                            Text(
                                widget._task.title,
                                style: TextStyle(
                                fontSize: 24
                                )
                            ),
                            Text(
                                widget._task.description,
                                style: TextStyle(
                                fontSize: 16
                                )
                            )
                        ]
                    )
                ),
                Chip(label: Text(taskStatus!))
            ]
        );

        

        Widget btnStart = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Start'),
                onPressed: () {
                    updateTask(context, 'Ongoing');
                },
                style: btnRed
            )
        );

        Widget btnFinish = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Finish'),
                onPressed: () { 
                    updateTask(context, 'Completed');
                },
                style: btnRed
            )
        );

        Widget btnAccept = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Accept'),
                onPressed: () {
                    updateTask(context, 'Accepted');
                },
                style: btnRed
            )
        );

        Widget btnReject = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Reject',
                style: TextStyle(
                        color: Colors.black
                )
                ),
                onPressed: () { 
                    updateTask(context, 'Rejected');
                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    side: BorderSide(width: 1.0, color: Colors.black,)  
                )
            )
        );

        Widget btnDetail = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: GestureDetector(
                child: Text(
                    'Detail',
                    style: TextStyle(
                        fontSize: 16,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                    )
                ),
                onTap: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailScreen(widget._task)));
                    widget._reloadTasks();
                },
            )
        );

        List<Widget> buttonOptions(){
            if(designation == 'assembly-team'){
                if(taskStatus == 'Pending'){
                    return [btnStart];
                } else if (taskStatus == 'Ongoing'){
                    return [btnFinish];
                } 
                
            } else if (designation == 'contractor') {
                if(taskStatus == 'Completed'){
                    return [btnAccept, btnReject] ;
                } else if (taskStatus == "Rejected"){
                    return [btnAccept];
                } else if (taskStatus == 'Accepted'){
                    return [btnReject];
                }
            }
            return [];

        }


        Widget rowActions = Row(
            children: [
                btnDetail,
                Spacer(),
                ...buttonOptions()
             
                // Show btnStart if designation is assembly team and task status is pending, else show an empty container.
                // Show btnFinish if designation is assembly team and task status is ongoing, else show an empty container.
                // Show btnAccept if designation is contractor and task status is completed or rejected, else show an empty container.
                // Show btnReject if designation is contractor and task status is completed or accepted, else show an empty container.
            ]
        );

        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        rowTaskInfo,
                        SizedBox(height: 16.0),
                        rowActions
                    ]
                )
            )
        );
    }
}