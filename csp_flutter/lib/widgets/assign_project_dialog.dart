import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/utils/api.dart';
import '/utils/functions.dart';
import '/providers/user_provider.dart';

class AssignProjectDialog extends StatefulWidget {
    final int? _projectId;

    AssignProjectDialog([ this._projectId ]);

    @override
    _AssignProjectDialog createState() => _AssignProjectDialog();
}

class _AssignProjectDialog extends State<AssignProjectDialog> {
    final _formKey = GlobalKey<FormState>();

    List<DropdownMenuItem> _subcontractorOptions = [];
    int? _assignedTo;

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;

            API(accessToken).getUsersByDesignation('subcontractors').then((subcontractors) {
                setState(() {
                    _subcontractorOptions = subcontractors.map((subcontractor) {
                        return DropdownMenuItem(
                            child: Text(subcontractor.email!),
                            value: subcontractor.id
                        );
                    }).toList();
                });
            }).catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        DropdownButtonFormField txtSubcontractor = DropdownButtonFormField(
            decoration: InputDecoration(labelText: 'Subcontractor'),
            items: _subcontractorOptions,
            onChanged: (value) {
                // Update the assignedTo state according to selected option.
                setState(() {
                    _assignedTo = value;                    
                });

            }
        );

        Widget formAssignProject = Form(
            key: _formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    txtSubcontractor
                ]
            )
        );

        return AlertDialog(
            title: Text('Assign Project'),
            content: Container(
                child: formAssignProject
            ),
            actions: [
                ElevatedButton(
                    child: Text('Assign'),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            API(accessToken).assignProject(assignedTo:_assignedTo, projectId: widget._projectId).catchError((error){
                            showSnackBar(context, error.message);    
                        });
                            Navigator.of(context).pop();
                        } else {
                            showSnackBar(context, 'Select a resolver to assign the task.');
                        }
                    },
                    style: ElevatedButton.styleFrom(
                        primary: Colors.red[700]
                    )
                ),
                ElevatedButton(
                    child: Text('Cancel',
                        style: TextStyle(
                            color: Colors.black
                        )
                    ),
                    onPressed: () {
                        Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        side: BorderSide(width: 1.0, color: Colors.black)
                    )
                ),
            ],
        );
    }
}