# Project Management App

# Setup Guide

1. Open the repository where the project is assign
2. Click the clone button 
3. Choose SSH 
4. Copy the `git clone <project_url>`
5. Open your terminal 
6. Make sure you have .git folder (`git init` to create the folder)
7. Paste the SSH 
8. `flutter pub get` or for manual install check the packages provided in this file
9. Make sure to run the API  `dart run bin/main.dart`
10. Enjoy and Happy Coding!

# Test User Credentials

| Email | Password |
| ------------- | ------------- |
| contractor@gmail.com | contractor |
| subcontractor@gmail.com | subcontractor |
| assembly-team@gmail.com | assembly-team 


# Flutter Version
  The flutter version used to make the project is Flutter 2.2.3 

# Packages

[http ^0.13.3](https://pub.dev/packages/dio)

- A powerful Http client for Dart, which supports Interceptors, Global configuration, FormData, Request Cancellation, File downloading, Timeout etc.

[provider: ^5.0.0](https://pub.dev/packages/provider/versions/5.0.0-nullsafety.5)

- A wrapper around InheritedWidget to make them easier to use and more reusable.

[shared_preferences: ^2.0.6](https://pub.dev/packages/shared_preferences)

- Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.).

[image_picker: ^0.8.0+3](https://pub.dev/packages/image_picker)

- A Flutter plugin for iOS and Android for picking images from the image library, and taking new pictures with the camera.

[flutter_dotenv ^5.0.0](https://pub.dev/packages/flutter_dotenv)

- Load configuration at runtime from a .env file which can be used throughout the application.

[url_launcher ^6.0.9](https://pub.dev/packages/url_launcher)

- A Flutter plugin for launching a URL. Supports iOS, Android, web, Windows, macOS, and Linux.

# App Features

## Login Feature

- Save token to app state
- Save toke to local storage

## Contractor

- Show all project
- Add project
- Assign project to sub contractor
- Review finished project tasks

## Subcontractor

- Show assigned projects
- Show project tasks
- Add task & assign to assembly team

## Assembly Team

- Show projects with assigned tasks
- Show project tasks
- Tag task as ongoing
- Tag task as completed

